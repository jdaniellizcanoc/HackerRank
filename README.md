# Proyecto de Ruta Más Corta Basado en Grafos para la Universidad Industrial de Santander

## Índice
1. [Descripción General](#descripcion-general)
2. [Variables del Problema](#variables-del-problema)
   - [Nodos y Aristas](#nodos-y-aristas)
   - [Umbral de Conexión](#umbral-de-conexion)
   - [Cálculo de Distancias](#calculo-de-distancias)
   - [Saltos Máximos](#saltos-maximos)
3. [Justificación del Diseño Actual](#justificacion-del-diseno-actual)
4. [Propuesta de Alternativa con Grafo Dirigido](#propuesta-de-alternativa-con-grafo-dirigido)
5. [Análisis de Complejidad](#analisis-de-complejidad)
6. [Avances Actuales](#avances-actuales)
7. [Espacios para Visualización](#espacios-para-visualizacion)

---

## 1. Descripción General <a name="descripcion-general"></a>
El proyecto busca resolver el problema de encontrar la ruta más corta entre dos edificios dentro de la **Universidad Industrial de Santander (UIS)**. Los edificios están representados como **nodos** y las conexiones posibles entre ellos como **aristas** sobre un plano digital del campus. Este proyecto pretende facilitar la navegación para estudiantes, especialmente los de primer semestre.

---

## 2. Variables del Problema <a name="variables-del-problema"></a>

### 2.1. Nodos y Aristas <a name="nodos-y-aristas"></a>
- **Nodos**:
  - Representan edificios en el campus.
  - Hay un total de **50 nodos**, cada uno ubicado mediante coordenadas `(x, y)` en un plano digital basado en una imagen del campus.
  
- **Aristas**:
  - Representan las posibles conexiones entre edificios.
  - Cada nodo tiene entre **4 y 5 aristas**, para un total aproximado de **196 aristas** en el grafo.
  - Se utilizan **aristas no dirigidas**, permitiendo que las conexiones sean bidireccionales.

### 2.2. Umbral de Conexión <a name="umbral-de-conexion"></a>
- **Definición**: 
  - Solo se consideran conexiones entre nodos que están a una distancia menor o igual a **250 píxeles**.
  
- **Justificación**:
  1. **Reducción de complejidad**:
     - Limitar las conexiones disminuye la densidad del grafo, reduciendo la cantidad de rutas posibles que el algoritmo necesita evaluar.
  2. **Representación física realista**:
     - En la vida real, edificios muy alejados no se conectan directamente, ya que las rutas podrían atravesar estructuras o espacios no transitables.

### 2.3. Cálculo de Distancias <a name="calculo-de-distancias"></a>
- **Fórmula**:
  \[
  d = \sqrt{(x_2 - x_1)^2 + (y_2 - y_1)^2}
  \]
  
- **Justificación**:
  - La distancia euclidiana es adecuada para calcular distancias en un espacio bidimensional (como un plano).
  - Es computacionalmente eficiente y suficientemente precisa para determinar la conectividad entre nodos.

### 2.4. Saltos Máximos <a name="saltos-maximos"></a>
- **Definición**: Se establece un límite de **13 saltos** para encontrar una ruta antes de retroceder y explorar un camino alternativo.
  
- **Justificación**:
  1. **Control de rutas infinitas**:
     - En grafos grandes y no dirigidos, sin restricciones, el algoritmo podría explorar rutas innecesarias, lo que aumenta el tiempo de ejecución.
  2. **Optimización**:
     - Este límite reduce la cantidad de exploraciones improductivas, especialmente en grafos con múltiples ciclos.

---

## 3. Justificación del Diseño Actual <a name="justificacion-del-diseno-actual"></a>
1. **Uso de conexiones limitadas (umbral)**:
   - Mantiene un balance entre la conectividad del grafo y la complejidad del algoritmo.
   - Representa mejor las limitaciones físicas y prácticas del entorno.

2. **Cálculo euclidiano de distancias**:
   - Es computacionalmente eficiente.
   - Al operar en un espacio bidimensional, proporciona una métrica precisa para evaluar la cercanía entre edificios.

3. **Restricción de saltos máximos**:
   - Mejora la eficiencia explorando caminos lógicos en menos tiempo.
   - Permite gestionar de manera eficiente los recursos computacionales en grafos con ciclos.

---

## 4. Propuesta de Alternativa con Grafo Dirigido <a name="propuesta-de-alternativa-con-grafo-dirigido"></a>
Para reducir aún más la complejidad, se propone un **grafo dirigido**. 

### Cambios Propuestos:
1. **Direcciones en las Aristas**:
   - Las aristas serán dirigidas, representando rutas en un solo sentido entre nodos.
   - Ejemplo: Si desde el Nodo A al Nodo B hay un camino más corto, se define la dirección A → B.

2. **Reducción de Conexiones**:
   - Al eliminar bidireccionalidad, se reduce a la mitad el número de aristas, disminuyendo el tamaño del grafo.
   - Esto reduce la exploración innecesaria.

### Beneficios:
- **Menor complejidad**: Reduce el número de rutas posibles, optimizando la búsqueda.
- **Mayor realismo**: Representa mejor rutas unidireccionales, como calles o senderos.

---

## 5. Análisis de Complejidad <a name="analisis-de-complejidad"></a>

### Complejidad Espacial
- **Cantidad de Nodos**: \( n = 50 \)
- **Cantidad de Aristas**: \( m = 196 \)

### Complejidad Temporal
- **Exploración del Grafo**:
  - En el peor caso, la búsqueda abarca todas las aristas y nodos.
  - Para algoritmos como Dijkstra:
    \[
    O((n + m) \log n)
    \]
  - Para 50 nodos y 196 aristas:
    \[
    O((50 + 196) \log 50) \approx O(246 \cdot 5.6) \approx O(1377)
    \]
  - La cantidad exacta de iteraciones dependerá de las conexiones activas entre nodos.

### Eficiencia del Umbral
- Limitar conexiones mediante el umbral de 250 píxeles reduce el número de aristas, disminuyendo el tamaño del grafo y acelerando los cálculos.

---

## 6. Avances Actuales <a name="avances-actuales"></a>
1. Implementación de nodos y aristas en un plano.
2. Cálculo de distancias mediante la fórmula euclidiana.
3. Definición de restricciones como umbral y saltos máximos.
4. Evaluación de la alternativa de grafo dirigido.

---

## 7. Espacios para Visualización <a name="espacios-para-visualizacion"></a>
A continuación, se incluirán representaciones gráficas del grafo:

### 7.1. Grafo sin Pesos
_Imagen del grafo inicial (sin pesos)._  
![Grafo sin Pesos](grafo_sin_pesos.svg)

### 7.2. Grafo con Pesos
_Imagen del grafo con pesos en las aristas._  
![Grafo con Pesos](grafo_con_pesos.svg)

### 7.3. Simulación (GIF)
_GIF de una simulación del algoritmo encontrando la ruta más corta._  
![Simulación del Algoritmo](grafo_animado_nodos_y_aristas.gif)

